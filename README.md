# GunGodot

This Godot addon is a proof of concept port of Gun, a distributed database written in Javascript.

To learn more about gun, visit: https://gun.eco/docs/

# GunGodot

Esse addon criado para a Godot Engine é uma prova de conceito de um port da biblioteca Gun, um banco de dados distribuido escritp em Javascript.

Para saber mais sobre o Gun, visite: https://gun.eco/docs/
