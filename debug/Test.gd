extends Control

onready var text_edit = $TextEdit

const TEST_KEY = "test"


func _on_Button_pressed():
	var test = {"value": text_edit.text}
	Gun.put(TEST_KEY, test)


func _on_Button2_pressed():	
	Gun.get(TEST_KEY)
	var data = yield(Gun, "get_data_ready")
	text_edit.text = data.value
