extends Node

const _GUN_PORT = 8080
const _GRAPH = {}
const _PEERS = {}


var wss = WebSocketServer.new()

func _ready():
	_connect_ws_signals()
	wss.listen(_GUN_PORT)
	print("server started")


func _process(delta):
	wss.poll()


func _connect_ws_signals():
	for ws_signal in wss.get_signal_list():
		var signal_method_name = "_ws_%s" % ws_signal.name
		if not self.has_method(signal_method_name):
			continue
		wss.connect(ws_signal.name, self, signal_method_name)
		print("connected signal %s to method %s" % [ws_signal.name, signal_method_name])


func _ws_client_close_request(id: int, code: int, reason: String):
	print("Client close request")


func _ws_client_connected(id: int, protocol: String):
	print("client connected")
	if not protocol.empty():
		print("Protocol: %s" % protocol)


func _ws_client_disconnected(id: int, was_clean=false):
	print("client disconnected")


func _ws_data_received(id: int):
	var data = parse_json(wss.get_peer(id).get_packet().get_string_from_utf8())
	if data.has("dam") and data["dam"] == "hi":
		print("Received handshake from %s" % id)
	else:
		print("data received")
		print(data)
