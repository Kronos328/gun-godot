class_name UUID

static func generate() -> String:
	var crypto = Crypto.new()
	var rand_bytes = crypto.generate_random_bytes(16)
	
	rand_bytes[6] = 0x40 | (rand_bytes[6] & 0xf)
	rand_bytes[8] = 0x80 | (rand_bytes[8] & 0x3f)
	
	var hex_str = rand_bytes.hex_encode()
	
	print(hex_str)
	print(hex_str.length())
	
	hex_str = hex_str.insert(8, "-")
	hex_str = hex_str.insert(13, "-")
	hex_str = hex_str.insert(18, "-")
	hex_str = hex_str.insert(23, "-")
	
	print(hex_str)
	
	return hex_str
