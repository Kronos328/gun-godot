tool
extends EditorPlugin

const GUN_AUTOLOAD_NAME = "Gun"
const GUN_AUTOLOAD_PATH = "res://addons/gun_godot/gun.gd"


func _enter_tree():
	add_autoload_singleton(GUN_AUTOLOAD_NAME, GUN_AUTOLOAD_PATH)


func _exit_tree():
	remove_autoload_singleton(GUN_AUTOLOAD_NAME)
